"""Metadata dictionary API."""

from bottle import request
from sqlalchemy import func, select, true

from passportstorage.api.common import Application
from passportstorage.api.common.errors import DictionaryNotFoundError
from passportstorage.api.schemes.dictionary_meta import (
    get_dictionary_fields_schema,
)
from passportstorage.db.tables import meta_dictionary, meta_dictionary_field

meta_dict_app = Application()
meta_dict_app.config['BASE_URL'] = '/meta-dictionary'


@meta_dict_app.get('/list', name='meta_dictionary_list')
def get_meta_dictionary_list(db):
    """Return a list of meta dictionaries.

    :param db: A database connection
    :type db: class:`sqlalchemy.engine.base.Connection`
    :return: Dictionary with the following structure:
        {'values': list({`code`: `str`, `displayName`: `str`})}
    :rtype: dict

    """
    # TODO: This can be wrapped with a plugin.
    query = select(
        [meta_dictionary.c.code, meta_dictionary.c.display_name],
        whereclause=meta_dictionary.c.use_for_api == true()
    )
    values = [
        {'code': row.code, 'displayName': row.display_name}
        for row in db.execute(query)
    ]
    return {'values': values}


@meta_dict_app.post(
    '/get-fields',
    name='meta_dictionary_field_list',
    schema=get_dictionary_fields_schema
)
def get_meta_dictionary_field_list(db):
    """Return a list of fields for specified meta dictionary.

    :param db: A database connection
    :type db: class:`sqlalchemy.engine.base.Connection`
    :return: Dictionary fields with the following structure:
        {'values': list({`code`: `str`, `displayName`: `str`,
         `dataType`: `str`})}
    :rtype: list

    """
    dictionary_code = request.json['dictionaryCode']
    query = (
        select([func.count(meta_dictionary.c.code)])
        .where(meta_dictionary.c.code == dictionary_code)
    )
    if db.execute(query).scalar() == 0:
        raise DictionaryNotFoundError(
            details={'dictionaryCode': dictionary_code}
        )

    response = {'values': []}
    query = (
        select([
            meta_dictionary_field.c.code,
            meta_dictionary_field.c.display_name,
            meta_dictionary_field.c.data_type,
        ])
        .where(meta_dictionary_field.c.dictionary_code == dictionary_code)
    )
    for row in db.execute(query):
        response['values'].append({
            'code': row.code,
            'displayName': row.display_name,
            'dataType': row.data_type
        })

    return response
