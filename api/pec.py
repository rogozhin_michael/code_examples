"""PEC strict reporting form (SRF) API."""

from passportstorage.api.common import (
    Application,
    ODSSearchQueryExecutor,
    ODSStatisticQueryExecutor,
    SearchQueryExecutor,
    StatisticQueryExecutor,
    get_search_query_context,
    get_statistic_query_context,
)
from passportstorage.api.schemes.pec import (
    get_pec_ods_schema,
    get_pec_schema,
    get_stat_pec_schema,
)
from passportstorage.core.querybuilder import LabelSource
from passportstorage.db.types import EntityType

pec_app = Application()
pec_app.config['BASE_URL'] = '/pec'


@pec_app.post(
    '/get-items',
    name='pec_list',
    schema=get_pec_schema
)
def get_pec(db):
    """Return a list of PEC strict reporting forms.

    :param db: A database connection
    :type db: class:`sqlalchemy.engine.base.Connection`
    :rtype: See `common.query_executor.SearchQueryExecutor.execute.__doc__`

    """
    query_context = get_search_query_context(
        db, EntityType.PEC, LabelSource.CODE
    )
    return SearchQueryExecutor(query_context).execute()


@pec_app.post(
    '/get-items-ods',
    name='pec_ods',
    schema=get_pec_ods_schema
)
def get_pec_ods(db):
    """Return a list of PEC strict reporting forms as ODS file.

    :param db: A database connection
    :type db: class:`sqlalchemy.engine.base.Connection`
    :rtype: See `common.query_executor.ODSSearchQueryExecutor.execute.__doc__`

    """
    query_context = get_search_query_context(
        db, EntityType.PEC, LabelSource.DISPLAY_NAME
    )
    return ODSSearchQueryExecutor(query_context).execute()


@pec_app.post(
    '/get-aggregation',
    name='stat_pec_list',
    schema=get_stat_pec_schema
)
def get_stat_pec(db):
    """Return the result of a statistical query to pec strict reporting forms.

    :param db: A database connection
    :type db: class:`sqlalchemy.engine.base.Connection`
    :rtype: See `common.query_executor.StatisticQueryExecutor.execute.__doc__`

    """
    query_context = get_statistic_query_context(db, EntityType.PEC)
    return StatisticQueryExecutor(query_context).execute()


@pec_app.post(
    '/get-aggregation-ods',
    name='stat_pec_list_ods',
    schema=get_stat_pec_schema
)
def get_stat_pec_ods(db):
    """Return the result of a statistical query to pec SRF as ODS file.

    :param db: A database connection
    :type db: class:`sqlalchemy.engine.base.Connection`
    :rtype: See
        `common.query_executor.ODSStatisticQueryExecutor.execute.__doc__`

    """
    query_context = get_statistic_query_context(db, EntityType.PEC)
    return ODSStatisticQueryExecutor(query_context).execute()
