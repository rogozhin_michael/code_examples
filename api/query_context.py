"""Query context module.

Contains classes that process the query parameters and provide the context
for its execution.

"""
from abc import ABC, abstractmethod
from collections import OrderedDict

import bottle
from sqlalchemy import or_, text, union_all

from passportstorage.api.common import errors
from passportstorage.api.common.utils import (
    build_query,
    build_report_datetime_filter,
    try_get_id_code,
)
from passportstorage.core import querybuilder
from passportstorage.core.querybuilder import builder
from passportstorage.core.querybuilder.selector import Function
from passportstorage.core.rbac import ObjectLinkedRole, RoleRegistry, roles
from passportstorage.db.metadata_wrapper import MetadataWrapper
from passportstorage.db.tables import (
    TableName,
    meta_military_object_field,
    meta_pec_field,
    meta_person_field,
    meta_request_field,
    meta_violation_field,
)
from passportstorage.db.types import EntityType, QueryType


def get_search_query_context(session, entity_type, label_source):
    """Return an instance of `SearchQueryContext`.

    :type session: class:`sqlalchemy.engine.base.Connection`
    :type entity_type: class:`db.types.EntityType`
    :type label_source: class:`core.querybuilder.LabelSource`
    :rtype: class:`SearchQueryContext`

    """
    login, query_params = _get_request_context()
    context_class = _search_query_context_map[entity_type]
    return context_class(session, login, label_source, query_params)


def get_statistic_query_context(session, entity_type):
    """Return an instance of `StatisticQueryContext`.

    :type session: class:`sqlalchemy.engine.base.Connection`
    :type entity_type: class:`db.types.EntityType`
    :rtype: class:`StatisticQueryContext`

    """
    login, query_params = _get_request_context()
    context_class = _statistic_query_context_map[entity_type]
    return context_class(session, login, query_params)


def _get_request_context():
    """Return a request context.

    :return: User login and body
    :rtype: tuple(str, dict)

    """
    login, _ = bottle.request.auth
    query_params = bottle.request.json
    return login, query_params


def _quote(string):
    """Return a string wrapped in quotes.

    :type string: str
    :rtype: str

    """
    return '\'{}\''.format(string)


def _with_alias(clause, alias):
    """Return a clause with alias for the SELECT statement.

    :param clause: raw SQL string
    :type clause: str
    :type alias: str
    :rtype: class:`sqlalchemy.sql.elements.TextClause`

    """
    aliased_clause = '{} AS "{}"'.format(clause, alias)
    return text(aliased_clause)


class QueryContext(ABC):
    """Abstract base class for query context.

    :param session: A database connection
    :type session: class:`sqlalchemy.engine.base.Connection`
    :param login: User login
    :type login: str
    :param query_params: Query parameters
    :type query_params: dict

    """

    entity_type = None
    query_type = None
    _table_name = None
    _meta_table = None
    _mo_id_column_name = None
    _linked_role = None

    def __init__(self, session, login, query_params):
        self.session = session
        self.login = login

        self.report_datetime = query_params.get('reportDateTime')

        metadata = MetadataWrapper(self.session.engine)
        self._table = metadata.get_table(
            self._table_name, exception=errors.InternalError()
        )
        self._mo_id_column = self._table.c.get(self._mo_id_column_name)
        if self._mo_id_column is None:
            raise RuntimeError(
                '`{}` table doesn\'t have `{}` column'.format(
                    self._table.name, self._mo_id_column_name
                )
            )

    @abstractmethod
    def get_build_context(self):
        """Return the context for building the query."""
        pass

    @abstractmethod
    def get_rbac_context(self):
        """Return the context for checking role access."""
        pass

    def _get_report_datetime_filter(self):
        """Return a filter by the report datetime.

        :rtype: class:`sqlalchemy.sql.elements.BinaryExpression`

        """
        return build_report_datetime_filter(self._table, self.report_datetime)

    def _get_mo_id_filter(self, mo_ids):
        """Return a filter by military object ids.

        :type mo_ids: list[str]
        :rtype: class:`sqlalchemy.sql.elements.BinaryExpression`

        """
        return self._mo_id_column.in_(mo_ids or [None])


class SearchQueryContext(QueryContext):
    """Abstract base class for the search query context.

    :param session: A database connection
    :type session: class:`sqlalchemy.engine.base.Connection`
    :param login: User login
    :type login: str
    :param label_source: Source where to get labels for columns
    :type label_source: class:`core.querybuilder.selectors.LabelSource`
    :param query_params: Query parameters
    :type query_params: dict

    """

    query_type = QueryType.SEARCH

    def __init__(self, session, login, label_source, query_params):
        super().__init__(session, login, query_params)
        self.paging_params = query_params.get('pagingOptions')
        self.column_codes = query_params.get('fields')
        self.filter_params = query_params.get('filter')
        self.sort_params = query_params.get('sortOptions')
        self.mo_ids = query_params.get('searchMilitaryObjects')
        self.label_source = label_source
        self.builder = builder.ObjectQueryBuilder(
            self.session,
            self._table,
            self._meta_table,
            report_datetime=self.report_datetime
        )

    def get_build_context(self):
        """Return the context for building the query.

        :rtype: dict

        """
        return {
            'builder': self.builder,
            'column_codes': self.column_codes,
            'label_source': self.label_source,
            'filter_params': self.filter_params,
            'sort_params': self.sort_params,
            'default_order_by_clauses': self._get_default_order_by_clauses(),
            'extra_where_clauses': [
                self._get_report_datetime_filter(),
                self._get_mo_id_filter(self.mo_ids)
            ]
        }

    def get_rbac_context(self):
        """Return the context for checking role access.

        :rtype: list

        """
        context = []
        if self.mo_ids:
            context.append((self._linked_role.code, self.mo_ids))
        return context

    @abstractmethod
    def _get_default_order_by_clauses(self):
        """Return order by clauses.

        :rtype: list

        """
        pass


class StatisticQueryContext(QueryContext):
    """Base class for the statistic query context.

    See `QueryContext.__doc__` for details.

    """

    query_type = QueryType.STATISTIC

    _GROUP_NAME_LABEL = 'group_name'
    _ORPHAN_COLUMNS = [_with_alias('NULL', _GROUP_NAME_LABEL)]
    _TOTAL_GROUP_NAME = 'Итого'
    _TOTAL_AGGREGATIONS_SET = {Function.SUM, Function.COUNT}

    def __init__(self, session, login, query_params):
        super().__init__(session, login, query_params)
        self.is_group_by = query_params.get('groupByMilitaryObject', False)
        self.is_include_total = query_params.get('includeTotal', False)
        self.aggregations = query_params.get('aggregations')
        self.mo_id_groups = query_params.get('searchMilitaryObjectGroups')
        self._builder = builder.ObjectStatQueryBuilder(
            self.session, self._table, self._meta_table, self.report_datetime
        )
        metadata = MetadataWrapper(self.session.engine)
        self._mo_table = metadata.get_table(
            TableName.MILITARY_OBJECT, exception=errors.InternalError()
        )

    def get_build_context(self):
        """Return the context for building the query.

        :rtype: dict

        """
        mo_ids, mo_id_groups = self._get_group_object_ids()
        mo_ids, mo_id_groups = self._filter_by_having_id(mo_id_groups, mo_ids)
        context = []
        group_params = self._get_group_params(mo_id_groups, mo_ids)
        report_datetime_filter = self._get_report_datetime_filter()
        for group_name, mo_filter, columns, agg_these_only in group_params:
            for params in self.aggregations:
                agg_func = Function(params.get('function'))
                if agg_these_only is None or agg_func in agg_these_only:
                    where_clauses = [report_datetime_filter, mo_filter]
                else:
                    where_clauses = [text('false')]
                aggr_name = params.get('name')
                aggr_name_column = _with_alias(_quote(aggr_name), 'aggr_name')
                context.append({
                    'builder': self._builder,
                    'columns': [*columns, aggr_name_column],
                    'function': agg_func,
                    'column_code': params.get('fieldCode'),
                    'filter_params': params.get('filter'),
                    'extra_where_clauses': where_clauses
                })
        return context

    def get_rbac_context(self):
        """Return the context for checking role access.

        :rtype: list

        """
        return []

    def _get_group_object_ids(self):
        """Return the IDs of the MO objects as well as their groups.

        :rtype: tuple(list[str], collections.OrderedDict)

        """
        queries = []
        mo_ids = []
        mo_id_groups = OrderedDict()
        mo_id_code = try_get_id_code(
            self.session, self._mo_table, meta_military_object_field
        )
        query_builder = builder.ObjectQueryBuilder(
            self.session, self._mo_table, meta_military_object_field
        )
        for group in self.mo_id_groups:
            group_name = group['name']
            query = build_query(
                query_builder,
                column_codes=[mo_id_code],
                label_source=querybuilder.LabelSource.CODE,
                filter_params=group['filter']
            )
            query.append_column(
                _with_alias(_quote(group_name), self._GROUP_NAME_LABEL)
            )
            queries.append(query.order_by(None))
            mo_id_groups[group_name] = []

        for mo_id, group_name in self.session.execute(union_all(*queries)):
            mo_ids.append(mo_id)
            mo_id_groups[group_name].append(mo_id)
        return mo_ids, mo_id_groups

    def _filter_by_having_id(self, mo_id_groups, mo_ids):
        """Filter the `mo_ids` to which user has an access role.

        :type mo_id_groups: collections.OrderedDict
        :type mo_ids: list[str]
        :raises TypeError:
            If role (that matches role_code) is not instance of
            :class:`core.rbac.role_types.ObjectLinkedRole`
        :rtype: tuple(list[str], collections.OrderedDict)

        """
        if not mo_ids:
            return mo_ids, mo_id_groups

        registry = RoleRegistry(self.session, self.login)
        user_role = registry.get_user_role(self._linked_role.code)
        if user_role:
            if not isinstance(user_role, ObjectLinkedRole):
                raise TypeError(
                    '`user_role` must be instance of `ObjectLinkedRole`'
                )
            has_access_mo_ids = user_role.object_ids
        else:
            has_access_mo_ids = []

        allowed_mo_ids = set(mo_ids) & set(has_access_mo_ids)
        allowed_mo_id_groups = mo_id_groups.copy()
        for group_value in allowed_mo_id_groups.values():
            group_value[:] = [
                mo_id for mo_id in group_value if mo_id in allowed_mo_ids
            ]
        return list(allowed_mo_ids), allowed_mo_id_groups

    def _get_group_params(self, mo_id_groups, mo_ids):
        """Return parameters for building a query for each group of objects.

        :type mo_id_groups: collections.OrderedDict
        :type mo_ids: list[str]
        :rtype: list[tuple]

        """
        if self.is_group_by:
            group_params = []
            for group_name, mo_ids_ in mo_id_groups.items():
                filter_ = self._get_mo_id_filter(mo_ids_)
                columns = [
                    _with_alias(_quote(group_name), self._GROUP_NAME_LABEL)
                ]
                group_params.append((group_name, filter_, columns, None))
            if self.is_include_total:
                filter_ = self._get_mo_id_filter(mo_ids)
                columns = [
                    _with_alias(_quote(self._TOTAL_GROUP_NAME), self._GROUP_NAME_LABEL)  # NOQA E501
                ]
                group_params.append((
                    self._TOTAL_GROUP_NAME,
                    filter_,
                    columns,
                    self._TOTAL_AGGREGATIONS_SET
                ))

        else:
            filter_ = self._get_mo_id_filter(mo_ids)
            group_params = [(None, filter_, self._ORPHAN_COLUMNS, None)]
        return group_params


class PersonCardSearchQueryContext(SearchQueryContext):
    """Person card search query context.

    See `SearchQueryContext.__doc__` for details.

    """

    entity_type = EntityType.PERSON_CARD
    _table_name = TableName.PERSON
    _meta_table = meta_person_field
    _mo_id_column_name = 'military_object_id'
    _linked_role = roles.read_linked_person_role

    def __init__(self, session, login, label_source, query_params):
        super().__init__(session, login, label_source, query_params)
        self.include_orphan = query_params.get('searchOrphanRecords', False)

    def get_build_context(self):
        """See `SearchQueryContext.get_build_context.__doc__` for details."""
        if not self.mo_ids and not self.include_orphan:
            return None
        return super().get_build_context()

    def get_rbac_context(self):
        """See `SearchQueryContext.get_rbac_context.__doc__`."""
        context = super().get_rbac_context()
        if self.include_orphan:
            context.append((roles.read_non_linked_person_role.code, None))
        return context

    def _get_mo_id_filter(self, object_ids):
        """See `QueryContext._get_mo_id_filter.__doc__` for details."""
        object_id_filter = super()._get_mo_id_filter(object_ids)
        if self.include_orphan:
            object_id_filter = or_(
                object_id_filter, self._mo_id_column.is_(None)
            )
        return object_id_filter

    def _get_default_order_by_clauses(self):
        """See `SearchQueryContext._get_default_order_by_clauses.__doc__`."""
        return [self._table.c.win.asc()]


class PECRequestSearchQueryContext(SearchQueryContext):
    """PEC request search query context."""

    entity_type = EntityType.REQUEST
    _table_name = TableName.REQUEST
    _meta_table = meta_request_field
    _mo_id_column_name = 'military_object_id'
    _linked_role = roles.read_linked_request_role

    def _get_default_order_by_clauses(self):
        return [self._table.c.create_time.desc()]


class PECSearchQueryContext(SearchQueryContext):
    """PEC SRF search query context."""

    entity_type = EntityType.PEC
    _table_name = TableName.PEC
    _meta_table = meta_pec_field
    _mo_id_column_name = 'military_object_id'
    _linked_role = roles.read_linked_pec_role

    def _get_default_order_by_clauses(self):
        return [self._table.c.entry_date.desc()]


class ViolationSearchQueryContext(SearchQueryContext):
    """Violation search query context."""

    entity_type = EntityType.VIOLATION
    _table_name = TableName.VIOLATION
    _meta_table = meta_violation_field
    _mo_id_column_name = 'military_object_id'
    _linked_role = roles.read_linked_violation_role

    def _get_default_order_by_clauses(self):
        return [self._table.c.create_time.desc()]


class MilitaryObjectSearchQueryContext(SearchQueryContext):
    """Military object search query context."""

    entity_type = EntityType.MILITARY_OBJECT
    _table_name = TableName.MILITARY_OBJECT
    _meta_table = meta_military_object_field
    _mo_id_column_name = 'id'
    _linked_role = roles.read_linked_military_object_role

    def _get_default_order_by_clauses(self):
        return [self._table.c.id.asc()]


class PersonCardStatisticQueryContext(StatisticQueryContext):
    """Person card statistic query context.

    See `StatisticQueryContext.__doc__` for details.

    """

    entity_type = EntityType.PERSON_CARD
    _table_name = TableName.PERSON
    _meta_table = meta_person_field
    _mo_id_column_name = 'military_object_id'
    _linked_role = roles.read_linked_person_role

    def __init__(self, session, login, query_params):
        super().__init__(session, login, query_params)
        self.include_orphan = query_params.get('searchOrphanRecords', False)

    def get_build_context(self):
        """See `StatisticQueryContext.get_build_context.__doc__`."""
        if not self.mo_id_groups and not self.include_orphan:
            return []
        return super().get_build_context()

    def get_rbac_context(self):
        """See `StatisticQueryContext.get_rbac_context.__doc__`."""
        context = super().get_rbac_context()
        if self.include_orphan:
            context.append((roles.read_non_linked_person_role.code, None))
        return context

    def _get_group_object_ids(self):
        """See `StatisticQueryContext._get_group_object_ids.__doc__`."""
        if not self.mo_id_groups:
            return [], OrderedDict()
        return super()._get_group_object_ids()

    def _get_group_params(self, mo_id_groups, mo_ids):
        """See `StatisticQueryContext._get_group_params.__doc__`."""
        group_params = super()._get_group_params(mo_id_groups, mo_ids)
        if not self.include_orphan:
            return group_params
        orphan_filter = self._mo_id_column.is_(None)
        if self.is_group_by:
            params = [(None, orphan_filter, self._ORPHAN_COLUMNS, None)]
            if self.is_include_total:
                (group_name, filter_, columns, agg_these_only) = group_params.pop()  # NOQA E501
                if group_name != self._TOTAL_GROUP_NAME:
                    raise RuntimeError()
                filter_ = or_(filter_, orphan_filter)
                params.append((group_name, filter_, columns, agg_these_only))
        else:
            group_name, filter_, columns, agg_these_only = group_params.pop()
            filter_ = or_(filter_, orphan_filter)
            params = [(group_name, filter_, columns, agg_these_only)]
        group_params.extend(params)
        return group_params


class PECRequestStatisticQueryContext(StatisticQueryContext):
    """PEC request statistic query context."""

    entity_type = EntityType.REQUEST
    _table_name = TableName.REQUEST
    _meta_table = meta_request_field
    _mo_id_column_name = 'military_object_id'
    _linked_role = roles.read_linked_request_role


class PECStatisticQueryContext(StatisticQueryContext):
    """PEC SRF statistic query context."""

    entity_type = EntityType.PEC
    _table_name = TableName.PEC
    _meta_table = meta_pec_field
    _mo_id_column_name = 'military_object_id'
    _linked_role = roles.read_linked_pec_role


class ViolationStatisticQueryContext(StatisticQueryContext):
    """Violation statistic query context."""

    entity_type = EntityType.VIOLATION
    _table_name = TableName.VIOLATION
    _meta_table = meta_violation_field
    _mo_id_column_name = 'military_object_id'
    _linked_role = roles.read_linked_violation_role


class MilitaryObjectStatisticQueryContext(StatisticQueryContext):
    """Military object statistic query context."""

    entity_type = EntityType.MILITARY_OBJECT
    _table_name = TableName.MILITARY_OBJECT
    _meta_table = meta_military_object_field
    _mo_id_column_name = 'id'
    _linked_role = roles.read_linked_military_object_role


_search_query_context_map = {
    EntityType.PERSON_CARD: PersonCardSearchQueryContext,
    EntityType.REQUEST: PECRequestSearchQueryContext,
    EntityType.MILITARY_OBJECT: MilitaryObjectSearchQueryContext,
    EntityType.PEC: PECSearchQueryContext,
    EntityType.VIOLATION: ViolationSearchQueryContext
}

_statistic_query_context_map = {
    EntityType.PERSON_CARD: PersonCardStatisticQueryContext,
    EntityType.REQUEST: PECRequestStatisticQueryContext,
    EntityType.MILITARY_OBJECT: MilitaryObjectStatisticQueryContext,
    EntityType.PEC: PECStatisticQueryContext,
    EntityType.VIOLATION: ViolationStatisticQueryContext
}
