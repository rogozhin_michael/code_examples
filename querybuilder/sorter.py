"""Sorters module.

This module contains query sorting operators.
A sorting operator is an object that constructs sorting clauses in a query.

"""
import enum
import logging
from abc import abstractmethod

from passportstorage.core.querybuilder.operator import (
    MetaResolvingResolvingOperator,
)

_logger = logging.getLogger(__name__)


class SortOrder(enum.Enum):
    """Sort order."""

    ASC = 'ASC'
    DESC = 'DESC'


class Sorter(MetaResolvingResolvingOperator):
    """Abstract base class for sorter types."""

    @abstractmethod
    def sort(self, *args, **kwargs):
        """Build a sort clause.

        :rtype: list[:class:`sqlalchemy.sql.elements.UnaryExpression`]

        """
        pass


class PrimaryKeySorter(Sorter):
    """Primary key sorter."""

    @staticmethod
    def get_params(order=None):
        """Return a dictionary of parameters.

        :rtype: dict

        """
        return {'order': order or SortOrder.ASC}

    def sort(self, order=SortOrder.ASC):
        """Build a sort clause.

        :type order: class:`querybuilder.sorters.SortOrder`, optional
        :rtype: list[:class:`sqlalchemy.sql.elements.UnaryExpression`]

        """
        order_by_clauses = []
        pk_columns = self._resolver.get_pk_columns()
        for column in pk_columns:
            clause = column.asc() if order is SortOrder.ASC else column.desc()
            order_by_clauses.append(clause)
        return order_by_clauses


class DefaultSorter(Sorter):
    """Default query sorter."""

    def __init__(self, resolver):
        super().__init__(resolver)
        self._pk_sorter = PrimaryKeySorter(resolver)

    @staticmethod
    def get_params(params=None):
        """Return a dictionary of parameters.

        :type params: list[dict], optional
        :rtype: dict

        """
        return {'params': params}

    def default_sort(self):
        """See `querybuilder.sorters.PrimaryKeySorter.sort.__doc__`."""
        return self._pk_sorter.sort(order=SortOrder.ASC)

    def sort(self, params=None):
        """Build a sort clause.

        If `params` is None then sorting by `default_order_by_clauses` will be
        performed otherwise by primary key.

        :param params: A list of dictionary with sorting parameters
            that contains:
                {
                    'fieldCode': `str`,
                    'direction': `str`
                }
            `direction` must be one of `ASC` and `DESC`
        :type params: list[dict], optional
        :rtype: list[:class:`sqlalchemy.sql.elements.UnaryExpression`]

        """
        _logger.debug('Sort query by: params=%s', params)
        order_by_clauses = []
        if params is None:
            return order_by_clauses
        column_codes = [param['fieldCode'] for param in params]
        columns_data = self._resolver.resolve(column_codes=column_codes)
        for param in params:
            column_code = param['fieldCode']
            column_data = columns_data.get(column_code)
            column = column_data.column
            if SortOrder(param['direction']) is SortOrder.ASC:
                clause = column.asc()
            else:
                clause = column.desc()
            order_by_clauses.append(clause)
            self.affected_data.add(column_data)
        return order_by_clauses
