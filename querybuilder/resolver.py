"""Resolvers module.

A resolver is an object that resolves tables and columns by their metadata.

"""
import enum
import logging
from abc import ABC, abstractmethod
from collections import defaultdict, namedtuple

from sqlalchemy import and_, select

from passportstorage.core.querybuilder.errors import (
    ColumnNotFoundError,
    ColumnsMetaNotFoundError,
    DictionaryMetaNotFoundError,
    TableNotFoundError,
)
from passportstorage.db.metadata_wrapper import MetadataWrapper
from passportstorage.db.tables import meta_dictionary, meta_dictionary_field

_logger = logging.getLogger(__name__)

DictionaryTableData = namedtuple(
    'DictionaryTableData',
    ['table', 'metadata']
)
DictionaryColumnData = namedtuple(
    'DictionaryColumnData',
    ['column', 'metadata']
)
ObjectColumnData = namedtuple(
    'ObjectColumnData',
    ['column', 'metadata', 'object_metadata', 'data_source', 'dict_metadata']
)
DictionaryTableMetadata = namedtuple(
    'DictionaryTableMetadata',
    ['code', 'display_name', 'table_name', 'pk_column_name']
)
DictionaryColumnMetadata = namedtuple(
    'DictionaryColumnMetadata',
    ['code', 'display_name', 'data_type', 'dict_code', 'column_name']
)
ObjectColumnMetadata = namedtuple(
    'ObjectColumnMetadata',
    ['code', 'display_name', 'data_type', 'dict_code', 'table_name',
     'column_name']
)


class DataSourceLocation(enum.Enum):
    """Data source locations.

    LOCAL_TABLE: main entity table
    DICTIONARY_TABLE: LOCAL_TABLE -> dictionary_table
    CHILD_TABLE:
        LOCAL_TABLE <- child_table
        LOCAL_TABLE -> INTERMEDIATE_TABLE <- CHILD_TABLE
        LOCAL_TABLE <- CHILD_TABLE -> DICTIONARY_TABLE
        LOCAL_TABLE -> INTERMEDIATE_TABLE <- CHILD_TABLE -> DICTIONARY_TABLE

    """

    UNKNOWN = 0
    LOCAL_TABLE = 1
    DICTIONARY_TABLE = 2
    CHILD_TABLE = 3


class Resolver(ABC):
    """Abstract base resolver.

    :param db: Database connection session
    :type db: class:`sqlalchemy.engine.base.Connection`

    """

    def __init__(self, db):
        self._db = db
        self._cache = {}
        self._metadata = MetadataWrapper(self._db.engine)

    @abstractmethod
    def resolve(self, *args, **kwargs):
        """Resolve input metadata into real columns and tables..."""
        pass

    def get_table(self, table_name):
        """Return a table by `table_name`.

        :type table_name: str
        :raises class:`querybuilder.errors.TableNotFoundError`:
            If the table was not found in the current schema
        :rtype: class:`sqlalchemy.sql.schema.Table`

        """
        return self._metadata.get_table(
            table_name, exception=TableNotFoundError(table_name)
        )


class ColumnResolver(Resolver):
    """Abstract column resolver.

    :param db: Database connection session
    :type db: class:`sqlalchemy.engine.base.Connection`
    :type table: class:`sqlalchemy.sql.schema.Table`, optional

    """

    def __init__(self, db, table=None):
        super().__init__(db)
        self._table = table

    @abstractmethod
    def get_column_codes(self):
        """Return all column codes.

        :rtype: list[str]

        """
        pass

    @staticmethod
    def get_column(table, column_name):
        """Return the `table` column by `column_name`.

        :type table: class:`sqlalchemy.sql.schema.Table`
        :type column_name: str
        :raises class:`querybuilder.errors.ColumnNotFoundError`:
            If there is no `column_name` column in the `table`
        :rtype: class:`sqlalchemy.sql.schema.Column`

        """
        column = table.columns.get(column_name)
        if column is None:
            _logger.error(
                'The `%s` table does not have a `%s` column',
                table.name,
                column_name
            )
            raise ColumnNotFoundError(column_name)
        return column

    def get_pk_columns(self):
        """Return a list of columns with the primary key.

        :raises class:`querybuilder.errors.ColumnNotFoundError`
        :rtype: class:`sqlalchemy.sql.base.ColumnCollection`

        """
        columns = self._table.primary_key.columns
        if not columns:
            _logger.error('`%s` table has no primary keys', self._table.name)
            raise ColumnNotFoundError(self._table.name)
        return columns

    def _extract_code_from_cache_key(self, key):
        """Extract the real column code from the cache key.

        :type key: str
        :rtype: str

        """
        return key

    @staticmethod
    def _check_resolved(requested_codes, resolved_codes):
        """Check that resolving was successful otherwise raise an exception.

        :type requested_codes: iterable[str]
        :type resolved_codes: iterable[str]
        :raises class:`querybuilder.errors.ColumnsMetaNotFoundError`:
            If `requested_codes` is empty or the lengths of `requested_codes`
            and `resolved_codes` is not equal
        :rtype: NoneType

        """
        if not resolved_codes:
            _logger.error(
                'No column metadata found for codes: %s', requested_codes
            )
            raise ColumnsMetaNotFoundError(requested_codes)
        if requested_codes and len(requested_codes) != len(resolved_codes):
            not_found_codes = [
                *set(requested_codes).difference(resolved_codes)
            ]
            _logger.error(
                'No column metadata found for codes: %s', not_found_codes
            )
            raise ColumnsMetaNotFoundError(not_found_codes)
        return None

    def _find_in_cache(self, keys=None):
        """Return columns data from cache.

        :type keys: list[str], optional
        :rtype: tuple(dict, list)

        """
        if not keys:
            return self._cache, []
        data = {}
        not_found = []
        for key in keys:
            column_data = self._cache.get(key)
            code = self._extract_code_from_cache_key(key)
            if column_data is not None:
                data[code] = column_data
            else:
                not_found.append(code)
        return data, not_found

    def _cache_data(self, data):
        """Cache the `data`.

        :type data: dict
        :rtype: NoneType

        """
        self._cache.update(data)
        return None


class DictionaryTableResolver(Resolver):
    """Dictionary table resolver.

    This resolver works with the metadata of dictionary tables not columns.

    """

    _meta_table = meta_dictionary

    def resolve(self, code):
        """Resolve dictionary data.

        :type code: str
        :rtype: class:`querybuilder.common.DictionaryTableData`

        """
        data = self._cache.get(code)
        if data:
            return data
        metadata = self._get_metadata(code)
        table = self.get_table(metadata.table_name)  # rename to table_name
        data = DictionaryTableData(table, metadata)
        self._cache[code] = data
        return data

    def _get_metadata(self, code):
        """Return the metadata of the dictionary table.

        :type code: str
        :raises class:`querybuilder.errors.DictionaryMetaNotFoundError`:
            If no dictionary metadata was found
        :rtype: class:`querybuilder.common.DictionaryTableMetadata`

        """
        query = select([self._meta_table], self._meta_table.c.code == code)
        dict_meta = self._db.execute(query).first()
        if dict_meta is None:
            _logger.error('dictionary with the code `%s` was not found', code)
            raise DictionaryMetaNotFoundError(code)
        metadata = DictionaryTableMetadata(
            code=dict_meta.code,
            display_name=dict_meta.display_name,
            table_name=dict_meta.db_table_name,
            pk_column_name=dict_meta.primary_key_field_code
        )
        return metadata


class DictionaryColumnResolver(ColumnResolver):
    """Dictionary column resolver.

    This resolver works with the metadata of dictionary columns.

    :type db: class:`sqlalchemy.engine.base.Connection`
    :type table: class:`sqlalchemy.sql.schema.Table`, optional
    :type dict_code: str, optional

    """

    _CACHE_KEY_SEP = '_'
    _meta_table = meta_dictionary_field

    # TODO: add the `use_cache` flag
    def __init__(self, db, table=None, dict_code=None):
        super().__init__(db, table=table)
        self._dict_code = dict_code

    def resolve(self, column_codes=None):
        """Resolve the column data of the bound dictionary table.

        :type column_codes: iterable[str], optional
        :rtype: dict

        """
        return self.unbound_resolve(
            self._table, self._dict_code, column_codes=column_codes
        )

    # TODO: It should be divided into two classes
    def unbound_resolve(self, dict_table, dict_code, column_codes=None):
        """Resolve the column data of the unbound dictionary table.

        :type dict_table: class:`sqlalchemy.sql.schema.Table`
        :type dict_code: str
        :type column_codes: iterable[str], optional
        :rtype: dict

        """
        cached_keys = [
            self._make_cache_key(dict_code, code)
            for code in column_codes or ()
        ]
        data, not_found = self._find_in_cache(cached_keys)
        if data and not not_found:
            return data
        codes_to_resolve = not_found or column_codes
        metadata = self._get_metadata(dict_code, column_codes=codes_to_resolve)
        for meta in metadata:
            column = self.get_column(dict_table, meta.column_name)
            data[meta.code] = DictionaryColumnData(column, meta)
        self._check_resolved(column_codes, data.keys())
        data_to_cache = {
            self._make_cache_key(dict_code, code): data
            for code, data in data.items()
        }
        self._cache_data(data_to_cache)
        return data

    def get_column_codes(self):
        """Return column codes.

        :rtype: list[str]

        """
        query = select(
            [self._meta_table.c.code],
            self._meta_table.c.dictionary_code == self._dict_code,
            order_by=self._meta_table.c.code.asc()
        )
        return [row.code for row in self._db.execute(query)]

    def _get_metadata(self, dict_code, column_codes=None):
        """Return the metadata of the dictionary columns.

        :type dict_code: str
        :type column_codes: iterable[str], optional
        :rtype: class:`querybuilder.common.DictionaryColumnMetadata`

        """
        query = select(
            [self._meta_table],
            self._meta_table.c.dictionary_code == dict_code
        )
        if column_codes:
            query.append_whereclause(
                self._meta_table.c.code.in_(column_codes)
            )
        metadata = []
        for row in self._db.execute(query):
            column_metadata = DictionaryColumnMetadata(
                code=row.code,
                display_name=row.display_name,
                data_type=row.data_type,
                dict_code=row.dictionary_code,
                column_name=row.db_column_name
            )
            metadata.append(column_metadata)
        return metadata

    def _extract_code_from_cache_key(self, key):
        """Extract the real column code from the cache key.

        :type key: str
        :raises ValueError: If the key format is incorrect
        :rtype: str

        """
        split_key = key.split(self._CACHE_KEY_SEP, 1)
        if len(split_key) != 2:
            raise ValueError('invalid cache key format: `{}`'.format(key))
        _, column_code = split_key
        return column_code

    def _make_cache_key(self, dict_code, column_code):
        """Make a key to save in the cache.

        :type dict_code: str
        :type column_code: str
        :rtype: str

        """
        return self._CACHE_KEY_SEP.join((dict_code, column_code))


class ObjectColumnResolver(ColumnResolver):
    """Object columns resolver.

    This resolver works with main entities not dictionaries.

    :param db: Database connection session
    :type db: class:`sqlalchemy.engine.base.Connection`
    :param table: Target table whose columns need to be resolved
    :type table: class:`sqlalchemy.sql.schema.Table`
    :param meta_table: Table with metadata of the `table`
    :type meta_table: class:`sqlalchemy.sql.schema.Table`

    """

    _DICT_COLUMN_CODE_SEP = '.'

    def __init__(self, db, table, meta_table):
        super().__init__(db, table=table)
        self._meta_table = meta_table
        self._dict_table_resolver = DictionaryTableResolver(self._db)
        self._dict_column_resolver = DictionaryColumnResolver(self._db)

    def resolve(self, column_codes=None):
        """Resolve the column data of the object table.

        :type column_codes: iterable[str], optional
        :raises TypeError: If the source of the data location could not be
            determined
        :rtype: dict

        """
        data, not_found = self._find_in_cache(column_codes)
        if data and not not_found:
            return data
        to_resolve = not_found or column_codes
        object_codes, dict_codes = self._parse_column_codes(to_resolve)
        metadata = self._get_metadata(column_codes=object_codes)

        for metadata_ in metadata:
            data_source = self._get_data_source(metadata_)
            if data_source is DataSourceLocation.LOCAL_TABLE:
                column_data = self._resolve_local_data(metadata_, data_source)
            elif data_source is DataSourceLocation.DICTIONARY_TABLE:
                column_data = self._resolve_dict_data(
                    metadata_,
                    data_source,
                    dict_codes.get(metadata_.code)
                )
            elif data_source is DataSourceLocation.CHILD_TABLE:
                column_data = self._resolve_child_data(
                    metadata_,
                    data_source,
                    dict_codes.get(metadata_.code)
                )
            else:
                raise TypeError(
                    'unknown data source: `{}`'.format(data_source)
                )
            data.update(column_data)
        self._check_resolved(column_codes, data.keys())
        self._cache_data(data)
        return data

    def get_column_codes(self):
        """Return column codes.

        :rtype: list[str]

        """
        query = (
            select(
                [self._meta_table.c.code],
                and_(
                    self._meta_table.c.dictionary_code.is_(None),
                    self._meta_table.c.db_table_name.is_(None)
                )
            )
            .order_by(self._meta_table.c.code.asc())
        )
        return [row.code for row in self._db.execute(query)]

    def _resolve_local_data(self, metadata, data_source):
        """Resolve data for a local column.

        :type metadata: class:`querybuilder.common.ObjectColumnMetadata`
        :type data_source: class:`querybuilder.resolver.DataSourceLocation`
        :rtype: dict

        """
        column = self.get_column(self._table, metadata.column_name)
        column_data = ObjectColumnData(
            column=column,
            metadata=metadata,
            object_metadata=metadata,
            data_source=data_source,
            dict_metadata=None
        )
        return {metadata.code: column_data}

    def _resolve_dict_data(self, metadata, data_source, column_codes=None):
        """Resolve data for dictionary columns.

        :type metadata: class:`querybuilder.common.ObjectColumnMetadata`
        :type data_source: class:`querybuilder.resolver.DataSourceLocation`
        :type column_codes: iterable[str], optional
        :raises class:`querybuilder.errors.ColumnsMetaNotFoundError:
            If the metadata of the dictionary columns was not found
        :rtype: dict

        """
        dict_data = self._dict_table_resolver.resolve(metadata.dict_code)
        try:
            columns_data = self._dict_column_resolver.unbound_resolve(
                dict_data.table,
                dict_data.metadata.code,
                column_codes=column_codes
            )
        except ColumnsMetaNotFoundError as error:
            not_found_codes = [
                self._make_dict_code(metadata.code, code)
                for code in error.args[0]
            ]
            error.args = (not_found_codes,)
            raise
        data = {}
        for code, data_ in columns_data.items():
            column_data = ObjectColumnData(
                column=data_.column,
                metadata=data_.metadata,
                object_metadata=metadata,
                data_source=data_source,
                dict_metadata=dict_data.metadata
            )
            code_ = self._make_dict_code(metadata.code, data_.metadata.code)
            data[code_] = column_data
        return data

    def _resolve_child_data(self, metadata, data_source, column_codes=None):
        """Resolve data for a child column.

        :type metadata: class:`querybuilder.common.ObjectColumnMetadata`
        :type data_source: class:`querybuilder.resolver.DataSourceLocation`
        :type column_codes: iterable[str], optional
        :rtype: dict

        """
        data = {}
        if metadata.dict_code:
            data.update(
                self._resolve_dict_data(metadata, data_source, column_codes)
            )
        else:
            table = self.get_table(metadata.table_name)
            column = self.get_column(table, metadata.column_name)
            data[metadata.code] = ObjectColumnData(
                column=column,
                metadata=metadata,
                object_metadata=metadata,
                data_source=data_source,
                dict_metadata=None
            )
        return data

    def _make_dict_code(self, object_column_code, dict_column_code):
        """Return the code of the dictionary column.

        :type object_column_code: str
        :type dict_column_code: str
        :rtype: str

        """
        return '{}{}{}'.format(
            object_column_code, self._DICT_COLUMN_CODE_SEP, dict_column_code
        )

    @staticmethod
    def _get_data_source(metadata):
        """Return the location of the column data corresponding to the meta.

        :type metadata: class:`querybuilder.common.ObjectColumnMetadata`
        :rtype: class:`querybuilder.resolver.DataSourceLocation`

        """
        if metadata.table_name:
            return DataSourceLocation.CHILD_TABLE
        if metadata.dict_code:
            return DataSourceLocation.DICTIONARY_TABLE
        return DataSourceLocation.LOCAL_TABLE

    def _get_metadata(self, column_codes=None):
        """Return columns metadata from the database.

        :type column_codes: iterable[str], optional
        :rtype: list[:class:`querybuilder.common.ObjectColumnMetadata`]

        """
        query = select([self._meta_table])
        if column_codes:
            query.append_whereclause(self._meta_table.c.code.in_(column_codes))
        metadata = []
        for row in self._db.execute(query):
            data = ObjectColumnMetadata(
                code=row.code,
                display_name=row.display_name,
                data_type=row.data_type,
                dict_code=row.dictionary_code,
                table_name=row.db_table_name,
                column_name=row.db_column_name
            )
            metadata.append(data)
        return metadata

    def _parse_column_codes(self, codes):
        """Parse column codes.

        Returns a pair of containers where the first one contains normalized
        entity codes and the second one contains dictionary column codes
        grouped by dictionary code.

        :type codes: iterable[str]
        :rtype: tuple(set, collections.defaultdict(list))

        """
        object_codes = set(codes or ())
        dict_codes = defaultdict(list)
        for column_code in codes:
            if self._is_dictionary_code(column_code):
                object_codes.discard(column_code)
                code, dict_column_code = column_code.split(
                    self._DICT_COLUMN_CODE_SEP
                )
                object_codes.add(code)
                dict_codes[code].append(dict_column_code)
        return object_codes, dict_codes

    def _is_dictionary_code(self, code):
        """Check whether `code` is the dictionary column code.

        :type code: str
        :rtype: bool

        """
        return self._DICT_COLUMN_CODE_SEP in code
